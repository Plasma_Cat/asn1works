﻿using System;
using System.Text;
using System.IO;
using static System.IO.File;
using LearningASN1;
using Org.BouncyCastle.Asn1;

namespace ReadASN1
{
    class Program
    {
        static void Main(string[] args)
        {
            //read the file
            //parse data to asn1
            //Decode
            //string fp = "D:\\1\\1.asn";
            string fp2 = "D:\\1\\2.asn";

            //var bytes = File.ReadAllBytes(fp);
            //var str64 = Convert.ToBase64String(bytes);
            //var bts = Encoding.ASCII.GetBytes(str64);

            //Org.BouncyCastle.Asn1.Asn1StreamParser cr = new Asn1StreamParser(bts);
            //var cd = cr.ReadObject();
            //var enc = cd.ToAsn1Object().GetEncoded();

            //var ddata = Keys.csp.Decrypt(enc, false);
            //var res = Encoding.ASCII.GetString(ddata);
            //Console.WriteLine(res);

            var txt = ReadAllText(fp2, Encoding.ASCII);

            var strs = txt.Split(' ');
            var bts = new byte[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                bts[i] = byte.Parse(strs[i], System.Globalization.NumberStyles.HexNumber);
            }
            var cr = new Asn1StreamParser(bts);
            var cd = cr.ReadObject();
            var enc = cd.ToAsn1Object().GetEncoded();

            var ddata = Keys.csp.Decrypt(enc, false);
            var res = Encoding.ASCII.GetString(ddata);
            Console.WriteLine("2: " + res);
            Console.ReadLine();
        }
    }
}
