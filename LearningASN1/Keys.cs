﻿using System.Security.Cryptography;

namespace LearningASN1
{
    /// <summary>
    /// Keys and other suppot info
    /// </summary>
    public static class Keys
    {
        /// <summary>
        /// lets take a new CSP with a new 2048 bit rsa key pair
        /// </summary>
        public static RSACryptoServiceProvider csp = new RSACryptoServiceProvider(2048);

        /// <summary>
        /// how to get the private key
        /// </summary>
        public static RSAParameters privKey = csp.ExportParameters(true);

        /// <summary>
        /// and the public key ...
        /// </summary>
        public static RSAParameters pubKey = csp.ExportParameters(false);

        private static string pubKeyString;

        /// <summary>
        /// converting the public key into a string representation
        /// </summary>
        public static string GetPubKeyString(RSAParameters pubKey_)
        {
            //we need some buffer
            var sw = new System.IO.StringWriter();
            //we need a serializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //serialize the key into the stream
            xs.Serialize(sw, pubKey_);
            //get the string from the stream
            pubKeyString = sw.ToString();
            return pubKeyString;
        }

        /// <summary>
        /// Converting string to the public key parametr back
        /// </summary>
        /// <param name="pubKeyString_"></param>
        /// <returns></returns>
        public static RSAParameters GetPubKeyFromString(string pubKeyString_)
        {
            //get a stream from the string
            var sr = new System.IO.StringReader(pubKeyString_);
            //we need a deserializer
            var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
            //get the object back from the stream
            pubKey = (RSAParameters)xs.Deserialize(sr);
            return pubKey;
        }

        /// <summary>
        /// Static constructor
        /// </summary>
        static Keys()
        {
            csp = new RSACryptoServiceProvider(2048);
            privKey = csp.ExportParameters(true);
            pubKey = csp.ExportParameters(false);
            GetPubKeyString(pubKey);

            csp.ImportParameters(pubKey);
        }

    }
}
