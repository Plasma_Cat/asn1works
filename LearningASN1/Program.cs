﻿using System;
using System.IO;
using static System.IO.File;
using System.Text;
using System.Security.Cryptography;
using System.Threading;
using org.bn;

namespace LearningASN1
{
    class AsnEncodedDataSample
    {
        static void Main()
        {
            //The following example demonstrates the usage the AsnEncodedData classes.
            // Asn encoded data is read from the extensions of an X509 certificate.
            try
            {

                Console.Write("Enter text to encode: ");
                var sorc = Console.ReadLine();

                Oid nOid = new Oid("1.3.6.1.4.1.343", "Intel Corporation");
                AsnEncodedData asndata = new AsnEncodedData(nOid, GetRawData(sorc));

                //int waiting = 300;
                //int attempts = 4;
                string fp_ = "D:\\1\\1.asn";
                byte[] bytes = Convert.FromBase64String(asndata.Format(true));
                WriteAllBytes(fp_, bytes);
                WriteAllText("D:\\1\\2.asn", asndata.Format(true), Encoding.ASCII);

                //byte[] octets = System.Text.Encoding.ASCII.GetBytes(asndata.Format(true));

                //using (var fs1 = GetFS(fp_, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write, ref attempts, waiting))
                //{
                ////    bytes = Encoding.ASCII.GetBytes(asndata.Format(true));
                //    fs1.Write(octets, 0, octets.Length);
                //}
                Console.WriteLine("Encodet data successfully write to the file. Hit any key.");
            }
            catch (CryptographicException)
            {
                Console.WriteLine("Encodet data not write to the file! Hit any key.");
            }
            Console.ReadLine();
        }

        /// <summary>
        /// Get raw data from string for AsnEncodedData.RawData
        /// </summary>
        /// <param name="sourcetext"></param>
        /// <returns>Encrypted raw data</returns>
        public static byte[] GetRawData(string sourcetext)
        {
            //for encryption, always handle bytes...
            var bytesPlainTextData = Encoding.ASCII.GetBytes(sourcetext);
            //apply pkcs#1.5 padding and encrypt our data 
            var rd = Keys.csp.Encrypt(bytesPlainTextData, false);

            return rd;
        }

        /// <summary>
        /// Try to get FileStream
        /// </summary>
        /// <param name="fp">Path</param>
        /// <param name="fm">File mode</param>
        /// <param name="fa">File Access</param>
        /// <param name="fs">File Share</param>
        /// <param name="att">Count of attempts</param>
        /// <param name="w">Waiting between attempts</param>
        /// <returns>FileStram Object</returns>
        public static FileStream GetFS(string fp, FileMode fm, FileAccess fa, FileShare fs, ref int att, int w)
        {
            try
            {
                return Open(fp, fm, fa, fs);
            }
            catch (UnauthorizedAccessException)
            {
                if (att <= 0)
                {
                    throw new UnauthorizedAccessException();
                }

                Thread.Sleep(w);
                att--;
                return GetFS(fp, fm, fa, fs, ref att, w);
            }
        }
    }
}